

$(function() {
    
    var classHide = 'hideBtn';
    var classShow = 'showBtn';

    var toggleBefore = function (element) {
	if (element.hasClass(classHide)) {
	    element.removeClass(classHide)
	    element.addClass(classShow)
	} else {
	    element.removeClass(classShow)
	    element.addClass(classHide)
	}
    }

    $('h4.solution').addClass(classHide);
    $('h4.solution').next().hide();
    $('h4.solution').css('cursor', 'pointer');
    $('h4.solution').click(function(){
	$(this).next().toggle();
	toggleBefore($(this));
    });
});


$(function() {
    
    $("div#toc > ul > ul").hide();

    $("div#text-prefixes > pre.example").hide();

});
