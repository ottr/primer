
(setq debug-on-error t)

(add-to-list 'load-path (expand-file-name "./org-mode/lisp"))
(add-to-list 'load-path (expand-file-name "./org-mode/contrib/lisp" t))

(require 'org)
(require 'ox-html)
(require 'ob)
(require 'ob-exp)
(require 'ob-tangle)

;; source block languages
(org-babel-do-load-languages
      'org-babel-load-languages
      '(
	(dot . t)
	(sh . t)
	(python . t)
	))

;; allow all evaluations of source blocks without confirmation:
(setq org-confirm-babel-evaluate nil)

